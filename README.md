# RSS Aggregator (RSSAGG)

## Overview
RSSAGG is a project developed in Golang that serves as an RSS aggregator API. It fetches RSS feeds from various sources, stores them in a PostgreSQL database, and exposes them through a RESTful API. The application is designed to be lightweight and efficient, making it suitable for deployment on resource-constrained devices like the Raspberry Pi 4. It utilizes Goose for fetching and parsing RSS feeds, SQLC for managing SQL queries and migrations, and Docker for containerization.

## Features
- Fetches RSS feeds from various sources.
- Stores fetched data in a PostgreSQL database.
- Provides RESTful API endpoints for accessing aggregated RSS data.
- Dockerized for easy deployment and management.
- Compatible with Raspberry Pi 4 for low-power consumption.
- Allows for adding, updating, and deleting RSS feeds dynamically.

## Technologies Used
- **Golang**: Primary programming language for development.
- **Goose**: Library for fetching and parsing RSS feeds.
- **SQLC**: Tool for generating SQL code and managing migrations.
- **PostgreSQL**: Database for storing aggregated RSS data.
- **Docker**: Containerization for easy deployment.
- **Raspberry Pi 4**: Hardware platform for running the application.
- **Docker Compose**: For orchestrating multiple Docker containers.

## Prerequisites
Before running the application, ensure the following prerequisites are met:
- Docker installed on your local machine.
- Docker Compose installed on your local machine.
- Raspberry Pi 4 set up and connected to the local network.
- Basic knowledge of Docker and Golang.