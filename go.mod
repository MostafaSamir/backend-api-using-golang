module gitlab.com/MostafaSamir/backend-api-using-golang

go 1.21.5

require (
	github.com/go-chi/chi v1.5.5
	github.com/go-chi/cors v1.2.1
	github.com/google/uuid v1.6.0
	github.com/joho/godotenv v1.5.1
)

require (
	github.com/lib/pq v1.10.9
	github.com/pressly/goose/v3 v3.19.2
)

require (
	github.com/mfridman/interpolate v0.0.2 // indirect
	github.com/sethvargo/go-retry v0.2.4 // indirect
	go.uber.org/multierr v1.11.0 // indirect
	golang.org/x/sync v0.6.0 // indirect
)
